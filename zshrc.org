#+TITLE: Zsh Config
#+PROPERTY: header-args :tangle .zshrc

* Table of Contents :toc:
- [[#path-to-your-oh-my-zsh-installation][Path to your oh-my-zsh installation.]]
- [[#set-theme][Set Theme]]
- [[#oh-my-zsh-settings][Oh-My-Zsh settings]]
- [[#plugins][Plugins]]
- [[#resourcing-oh-my-zsh][Resourcing oh-my-zsh]]
- [[#exports][Exports]]
- [[#print-out-aliases][Print out aliases]]
  - [[#general-aliases][General aliases]]
  - [[#lsd-aliases][Lsd aliases]]
  - [[#apps-aliases][Apps aliases]]
  - [[#emacs-aliases][Emacs aliases]]
  - [[#goto-directory-aliases][Goto Directory aliases]]
  - [[#emacs-bookmarks-aliases][Emacs Bookmarks aliases]]
- [[#aliases][Aliases]]
  - [[#general][General]]
  - [[#lsd][Lsd]]
  - [[#git][Git]]
  - [[#apps][Apps]]
  - [[#emacs][Emacs]]
  - [[#goto-directories][Goto Directories]]
  - [[#emacs-bookmarks][Emacs Bookmarks]]

* Path to your oh-my-zsh installation.
#+begin_src shell
export ZSH="/c/Users/Sujal Bajracharya/.oh-my-zsh"
#+end_src

* Set Theme
#+begin_src shell
ZSH_THEME="robbyrussell"
#+end_src

* Oh-My-Zsh settings
#+begin_src shell
HYPHEN_INSENSITIVE="true"
DISABLE_UPDATE_PROMPT="true"
DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
HIST_STAMPS="yyyy-mm-dd"
DISABLE_UPDATE_PROMPT="true"
#+end_src

* Plugins
#+begin_src
plugins=(
    git
)
#+end_src

* Resourcing oh-my-zsh
#+begin_src shell
source $ZSH/oh-my-zsh.sh
#+end_src

* Exports
#+begin_src shell
export LANG=en_US.UTF-8
export EDITOR='emacsclientw -c'
export VISUAL="emacsclientw -c -a runemacs"
export ALTERNATE_EDITOR="emacsclientw -c -a runemacs"
#+end_src

* Print out aliases
** General aliases
#+begin_src shell
gal() {
    echo "h  => show history of commands"
    echo "du => show size of item in current directory"
    echo "df => show size info of devices harddisk"
    echo "r  => source bashrc"
}
#+end_src

** Lsd aliases
#+begin_src shell
lal() {
    echo "la => show all item in directory"
    echo "ld => detailed output"
    echo "ld => detailed output with hidden items"
    echo "lx => sort ls output by extension."
    echo "lk => sort ls output by size, biggest last."
    echo "lr => output ls recursively."
}

#+end_src

** Apps aliases
#+begin_src shell
aal() {
    echo "ex  => explorer here"
    echo "vp  => open vpn"
    echo "vlc => open vlc"
    echo "sp  => run speedtest"
}
#+end_src

** Emacs aliases
#+begin_src shell
eal() {
    echo "emacw  => emacsclient in new frame"
    echo "emacsw => connet to open frame"
    echo "e      => runemacs"
}
#+end_src

** Goto Directory aliases
#+begin_src shell
dal() {
    echo "nc => nvim config"
    echo "ec => doom emacs config"
    echo "it => projects directory"
    echo "og => org directory"
}
#+end_src

** Emacs Bookmarks aliases
#+begin_src shell
bal() {
    echo "ze => zshrc"
    echo "ve => viebrc"
    echo "se => starhip config"
}
#+end_src

* Aliases
** General
#+begin_src shell
alias cls="clear"
alias c="clear"
alias c:='cd c:'
alias d:='cd d:'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'
alias md='mkdir'
alias rd='rm -rf'
alias mf='touch'
alias rf='rm'
alias h='history'
alias ..='cd ..'
alias du='du -kh'
alias df='df -kTh'
alias r='source ~/.zshrc'
#+end_src
** Lsd
#+begin_src shell
alias ls='lsd --color always'
alias lls='lsd -1'
alias la='ls -a'
alias lx='ls -X'
alias lk='ls -S'.
alias ld="ls -l --group-dirs first"
alias lh="ls -a -l --group-dirs first"
alias lr='ld --tree'
#+end_src
** Git
#+begin_src shell
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias config='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
#+end_src
** Apps
#+begin_src shell
alias vlc='/c/Program\ Files/VideoLAN/VLC/vlc.exe'
alias sp='speedtest'
alias vp='psiphon'
alias doom='~/.emacs.d/bin/doom'
alias ex='explorer .'
#+end_src
** Emacs
#+begin_src shell
alias emacsw='emacsclientw'
alias emacw='emacsclientw -c'
alias e='runemacs'
#+end_src
** Goto Directories
#+begin_src shell
alias it='cd ~/Desktop/Projects'
alias og='cd ~/Desktop/Org'
alias nc='cd ~/AppData/Local/nvim'
alias ec='cd ~/.doom.d'
#+end_src
** Emacs Bookmarks
#+begin_src shell
alias ze='emacsclientw -c ~/zshrc.org'
alias ve='emacsclientw -c ~/.vieb/viebrc'
#+end_src
